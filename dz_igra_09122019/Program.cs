﻿using System;
using static System.Console;

namespace ConsolePovtorenie
{
    class Program
    {
        static void Main()
        {
            int bank, stavka;
            Random rnd = new Random();
            int chislo1, chislo2, chislo3;
            int kombinTrue, kombinFalse;
            int bankPotrach;
            char restartGame;

            kombinTrue = kombinFalse = 0;
            bankPotrach = 0;

            do
            {

                Clear();
                Write("Введите сумму денег для игры (bank): ");
                bank = Convert.ToInt32(ReadLine());

                do
                {

                    Write("\nДелайте вашу ставку (" + bank + "): ");
                    stavka = Convert.ToInt32(ReadLine());

                    bank = bank - stavka;

                    WriteLine("\nКрутить?...");
                    ReadKey();

                    chislo1 = rnd.Next(10);
                    chislo2 = rnd.Next(10);
                    chislo3 = rnd.Next(10);

                    WriteLine("Ваша комбинация: " + chislo1 + " " + chislo2 + " " + chislo3);

                    if (chislo1 == 7 && chislo2 == 7 && chislo3 == 7)
                    {
                        stavka = stavka * 10;
                        kombinTrue = kombinTrue + 1;
                    }
                    else if (chislo1 == chislo2 && chislo2 == chislo3)
                    {
                        stavka = stavka * 3;
                        kombinTrue = kombinTrue + 1;
                    }
                    else if (chislo1 == chislo3)
                    {
                        stavka = stavka * 2;
                        kombinTrue = kombinTrue + 1;
                    }
                    else
                    {
                        bankPotrach = bankPotrach + stavka;
                        stavka = 0;
                        kombinFalse = kombinFalse + 1;
                    }

                    bank = bank + stavka;
                    WriteLine("\nТекущая сумма: " + bank);

                } while (bank != 0);

                WriteLine("Ваша статистика:");
                WriteLine($"Денег потрачено: {bankPotrach}\nУспешных комбинаций: {kombinTrue}\nНеуспешных комбинаций: {kombinFalse}");

                WriteLine("\nВаш bank равен = " + bank);
                Write(". Хотите начать игру заново: Y - да, N - нет: ");
                restartGame = Convert.ToChar(ReadLine());

            } while (restartGame == 'Y');

            ReadKey();
        }
    }
}
